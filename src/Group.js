import React, { Component } from 'react';
import './group.css';

class Group extends Component {
    render() {
        return(
        <div>
            <h2>{this.props.title}</h2>
            <div class="groupscroll" >
               {this.props.children}
            </div>
        </div>
        )
    }
}

export default Group;
import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import PlanetList from './planets/Planet';
import Galaxies from './galaxies/galaxies';
import Astronauts from './Astronauts/Astronauts';
import Inventory from './Inventory/Inventory';
import Rocket from './rocket/Rocket'
import './common/css/bootstrap.min.css'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedGalaxy: 0,
      availableGalaxies: []
    }
  }

  updateAvailableGalaxies = (availableGalaxies) =>{
    this.setState({
      availableGalaxies : availableGalaxies
    })
  }

  render() {
    return (
      <div className="App">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
      </link>
        <header className="App-header" style={{backgroundImage: `url(${"http://robotgalaxykids.co/wp-content/uploads/revslider/original-home1/banner_bg-2.jpg"})`, backgroundRepeat: "no-repeat"}}>
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Gotta catch em all!</h1>
        </header>      
        <div className="container">
          <Galaxies setAvailableGalaxies={this.updateAvailableGalaxies} actionClickCard={this.clickOnGalaxy} />
          <PlanetList galaxyId={this.state.selectedGalaxy} availableGalaxies={this.state.availableGalaxies} />
          <Rocket/>
          <Astronauts/>
          <Inventory />
        </div>  
      </div>
    );
  }

  clickOnGalaxy = (galaxyId) => {
      this.setState({
        selectedGalaxy: galaxyId
      })
      console.log("Galaxy id: " + galaxyId);
  }
}

export default App;

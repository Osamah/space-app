import React, { Component } from 'react';
import Astronaut from './Astronaut';
import AddAstronaut from './AddAstronaut';
import './Astronaut.css';
import './Form.css';
import Group from '../Group';
import { BASE_URL } from '../common/constants';



class Astronauts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            astronauts: []
        };
    }

    componentDidMount() {
        fetch(`${BASE_URL}/astronauts`)
            .then(function (response) {
                return response.json();
            })
            .then(myJson => {
                console.log(myJson);
                this.setState({ astronauts: myJson })

            });
    }

    // componentWillReceiveProps(props) {
    //     this.setState({
    //         astronauts: props.astronauts
    //     });
    // }

    deleteAstro = (idToDelete) => {
        fetch(`${BASE_URL}/astronauts/${idToDelete}`, {
            method: 'DELETE',
        }).then(res => {
            var copyAstronauts = [...this.state.astronauts]
            this.setState({
                astronauts: copyAstronauts.filter(obj => {
                    return obj.id !== idToDelete
                })
            });
        }).catch(error => console.error('Error:', error));

    }

    addAstro = (astronaut) => {
        console.log(astronaut);
        astronaut.id = this.state.astronauts[this.state.astronauts.length - 1].id + 1;
        fetch(`${BASE_URL}/astronauts`, {
            method: 'POST',
            body: JSON.stringify(astronaut),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            console.log(res);
            res.json();

        })
            .then(response => {
                this.setState((astronauts) => {
                    return {
                        astronauts: [...this.state.astronauts, astronaut]
                    }
                });
            })
            .catch(error => console.error('Error:', error));

    }

    render() {
        return (
            <div>
                <Group title="Choose your Astronaut">
                <div className="flexContainer">
                    {
                        this.state.astronauts.map(astronautFromList => <Astronaut astronaut={astronautFromList} deleteAstro={this.deleteAstro}></Astronaut>)
                    }

                </div>
                </Group>
                <AddAstronaut addAstro={this.addAstro}/>
            </div>
        )
    }
}

export default Astronauts;


import React, { Component } from 'react';
import './Astronaut.css';
class Astronaut extends Component {

    deleteAstro=()=>{
        this.props.deleteAstro(this.props.astronaut.id)
    }

    render() {
        const {Name,Age,Gender,Image,id}=this.props.astronaut;
        return (
            <div >
                <div className="card">
                <span onClick={this.deleteAstro}>&#9940;</span>
                    <img src={Image} alt="Avatar" style={{ width: "100%" }} />
                    <div className="container">
                        <h4><b>{Name}</b></h4>
                        <p>{Age}</p>
                        <p>{Gender}</p>
                    </div>
                </div>
                
            </div>
        )
    }
}
export default Astronaut
import React, { Component } from 'react';
import './Form.css';

class AddAstronaut extends Component {
    constructor(props) {

        super(props);
        this.refForm = React.createRef();

    }

    openForm = () => {
        this.refForm.current.style.display = "block";
    }

    closeForm = () => {
        this.refForm.current.style.display = "none";
    }

    handleChange = (event) => {
        var name = event.target.name;

        var value = event.target.value;
        this.setState({
            [name]: value
        })
    }

    addAstro = (event) => {

        event.preventDefault();

        var astro = {
            id: 0,
            Name: this.state.Name,
            Image: this.state.Img,
            Gender: this.state.Gender,
            Age: this.state.Age
        }
        this.props.addAstro(astro);
        this.closeForm();
    }

    render() {
        return (
            <div>
                <button className="open-button" onClick={this.openForm}>Add Astronaut</button>

                <div className="form-popup" id="myForm" ref={this.refForm}>
                    <form onSubmit={this.addAstro} className="form-container">
                        <h1>Login</h1>

                        <label htmlFor="name"><b>Name</b></label>
                        <input type="text" placeholder="Enter Astronaut Name" name="Name" onChange={this.handleChange} required />

                        <label htmlFor="img"><b>Image</b></label>
                        <input type="text" placeholder="Enter Image Link" name="Img" onChange={this.handleChange} required />

                        <label htmlFor="age"><b>Age</b></label>
                        <input type="number" placeholder="Enter Age" name="Age" onChange={this.handleChange} required />

                        <label htmlFor="gender"><b>Gender</b></label>
                        <select placeholder="Enter Gender" name="Gender" onChange={this.handleChange} required>
                            <option ></option>
                            <option value="Male" >Male</option>
                            <option value="Female">Female</option>
                        </select>

                        <button type="submit" className="btn">Add Astronaut</button>
                        <button type="button" className="btn cancel" onClick={this.closeForm}>Close</button>

                    </form>
                </div>
            </div>
        );
    }
}

export default AddAstronaut;
import React, { Component } from 'react';

class RocketItem extends Component {
  deleteRocket = () => {
    this.props.action(this.props.rocket.id);
  }
  render(){
    return (
      <div class="rocketitem">
      <div class="rocketdelete"><a onClick={this.deleteRocket}><img src="../../images/close_red.png"></img></a></div>
        <img src={this.props.rocket.image} alt={this.props.rocket.desc} />
        <p class="rocketimagedescription">{this.props.rocket.desc}</p>
        <a href={this.props.rocket.image}>Lees meer..</a>
      </div>
    );
  };
  }

export default RocketItem;
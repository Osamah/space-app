import React, { Component } from 'react';
// import myData from "../../db.json";
import RocketItem from './RocketItem.js';
import AddRocket from './AddRocket.js';
import AddRocketItem from './AddRocketItem.js';
import "./rocket.css";
import {BASE_URL} from '../common/constants';
import AddItem from '../AddItem/AddItem.js'

import Group from '../Group.js';

class Rocket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rockets: []
        }
    }

    addRocket = (rocket) => {
        const defaultRocket = {
            id: this.state.rockets.length + 1,
            image: "../../images/mark2.png",
            capacity: 1
        };

        const newRocket = {...defaultRocket, ...rocket};
        console.log(newRocket);
        this.setState({
            rockets: [...this.state.rockets, newRocket]
        });

        fetch(`${BASE_URL}/rockets`, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                body: JSON.stringify(newRocket), // body data type must match "Content-Type" header
                headers:{
                    'Content-Type': 'application/json'
                  }
                })
                .then(response => response.json()); // parses response to JSON
    }

    

    componentDidMount() {
        fetch(`${BASE_URL}/rockets`)
        .then(function(response) {
          return response.json();
        })
        .then(myJson => {
          this.setState({
              rockets: myJson
          }) 
        });
      }
    
      deleteRocket = (rocketId) => {

        fetch(`${BASE_URL}/rockets/${rocketId}`, {
            method: "DELETE", // *GET, POST, PUT, DELETE, etc.
            headers:{
                'Content-Type': 'application/json'
              }
            })
            .then(response => response.json()) // parses response to JSON
            
            var arrayRockets = [...this.state.rockets]; // make a separate copy of the array
            this.setState({rockets: arrayRockets.filter(rocket => rocket.id !== rocketId)});
      }

    render() {
        // const rocketDb = myData.rockets;
        return (
            // <div>
            //     <h2>Choose your rocket</h2>
            <div>
            <Group title="Choose your rocket">
                <div className="rocketitems">
                    {
                        this.state.rockets.map(rocket => <RocketItem rocket={rocket} action={this.deleteRocket}/>)
                    }
                    <AddRocketItem addRocket={this.addRocket}/>
                </div>
                <AddItem config={{itemAddName:"Rocket",inputNames:["name","desc"]}} addItem={this.addRocket}></AddItem>

            </Group>
            {/* <AddRocket addRocket={this.addRocket}/> */}
            </div>
            // </div>
        );
    }
}

export default Rocket;
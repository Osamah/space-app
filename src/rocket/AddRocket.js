import React, { Component } from 'react';


class AddRocket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: ''
        };
    
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescChange = this.handleDescChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleNameChange(event) {
        this.setState({name: event.target.value});
      }
      handleDescChange(event) {
        this.setState({description: event.target.value});
      }
    
      handleSubmit(event) {
        // alert('A name was submitted: ' + this.state.name);

        event.preventDefault();
        this.props.addRocket({
            name: this.state.name,
            desc: this.state.description
        });
      }
    
      render() {
        return (
          <form onSubmit={this.handleSubmit}>
            <label>
              Name:
              
    <input type="text" value={this.state.name} onChange={this.handleNameChange} />
            </label>
            <label>
              Description:
              
    <input type="text" value={this.state.description} onChange={this.handleDescChange} />
            </label>
            <input type="submit" value="Submit" />
          </form>
        );
      }
}

export default AddRocket;
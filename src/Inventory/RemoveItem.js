import React, { Component } from 'react';

class RemoveItem extends Component{
    render(){
        return(
            <div className="RemoveItem">
                {/* <input onChange = {this.setId} value = {this.state.Id} placeholder = "Id"></input> */}
                <button onClick ={this.removeItemFromInventory}>Delete item</button>
            </div>
        )
    }

    removeItemFromInventory = () => {
        this.props.removeItem(this.props.id)
    }

}

export default RemoveItem;
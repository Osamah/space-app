import React, { Component } from 'react';

class AddItem extends Component{
    constructor(props){
        super(props);

        this.state = {
            name: "",
            img:"",
            gewicht:"",
            grootte:"",
        }
    }
    render(){
        return(
            <div className = "addItem">
                <input onChange = {this.setName} value = {this.state.name} placeholder = "Name"></input>
                <input onChange = {this.setImg} value = {this.state.img} placeholder = "Img URL"></input>
                <input onChange = {this.setGewicht} value = {this.state.gewicht} placeholder = "Gewicht"></input>
                <input onChange = {this.setGrootte} value = {this.state.grootte} placeholder = "Grootte"></input>
                <button onClick ={this.addItemToInventory}>Create new item</button>
            </div>
        )
    }

  setName = (event) => {
        this.setState({
            name : event.target.value
        })
    }

    setImg = (event) => {
        this.setState({
            img : event.target.value
        })
    }

    setGewicht = (event) => {
        this.setState({
            gewicht : event.target.value
        })
    }

    setGrootte = (event) => {
        this.setState({
            grootte : event.target.value
        })
    }

    addItemToInventory = () => {
        this.props.createNewItem({
            name: this.state.name,
            img: this.state.img,
            gewicht: this.state.gewicht,
            grootte: this.state.grootte                 
        })
    }
}

    export default AddItem;
import React, { Component } from 'react';
import RemoveItem from './RemoveItem'
import '../common/styles.css';
import './Item.css';

class Item extends Component{
    render(){
        return(
        <div className = "card item">
            <img src = {this.props.content.img} alt = {this.props.content.name}/>
            <div className = "container">
                <h1>{this.props.content.name}</h1>            
                <h1>{this.props.content.gewicht}</h1>
                <h1>{this.props.content.grootte}</h1>
                <RemoveItem removeItem = {this.removeItem} />
            </div>
        </div>
        )
    }

    removeItem = () => {
        this.props.action(this.props.content.id)
    }
}

export default Item;
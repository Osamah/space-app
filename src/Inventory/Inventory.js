import React, { Component } from 'react';
import Item from './Item';
import SvensAddItem from './AddItem';
import '../common/styles.css';
import {BASE_URL} from '../common/constants';
import AddItem from '../AddItem/AddItem';
import Group from '../Group';

class Inventory extends Component{
    constructor(props){
        super(props);

        this.state ={
            items: []
        }
    }

    componentDidMount() {
        fetch(`${BASE_URL}/inventory`)
        .then(function(response) {
          return response.json();
        })
        .then(data => {
          this.setState({
              items: data
          })
        });
    }

    createNewItem = (item) => {
        
        fetch(`${BASE_URL}/inventory`,
        {
            method: 'POST',
            body: JSON.stringify(item),
            headers: {'Content-Type': 'application/json'}
        })
        .then(res => res.json())
        .then(response => {
            console.log(response)
            this.setState({
                items: [...this.state.items, response]
            });
        }).catch(error => console.error('Error', error));
    }

    removeItem = (id) => {
        var items = [...this.state.items];
        this.setState({
            items: items.filter(item => {
                return item.id !== parseInt(id, 10);
            })
        });

        fetch(`${BASE_URL}/inventory/${id}`, 
        {
            method: 'delete'
        })
        .then(responce => responce.json().then(json => {return json;}))
    }

    render(){
        return(
            <div>

                <Group title="Kies je voorwerpen">
                <div className = "flexContainer">
                    {this.state.items.map(item => <Item content={item} action = {this.removeItem}/>)}
                </div>
                <AddItem config={{itemAddName:"InventoryItem",inputNames:["name","img","gewicht","grootte"]}} addItem={this.createNewItem}></AddItem>

                </Group>
                {/* <SvensAddItem createNewItem={this.createNewItem} /> */}

              
            </div>
        )
    }
}


export default Inventory;
import React, { Component } from 'react';
import '../Astronauts/Form.css';



class AddItem extends Component {
    constructor(props) {

        super(props);
        this.refForm = React.createRef();
        this.formRefs=[]
        props.config.inputNames.forEach(element => {
            this.formRefs.push(React.createRef());
        });
    }

    openForm = () => {
        this.refForm.current.style.display = "block";
    }

    closeForm = () => {
        this.refForm.current.style.display = "none";
    }

    addItem = (event) => {
        event.preventDefault();
        const itemToAdd={}
       this.formRefs.map(ref=>{
            itemToAdd[ref.current.name]=ref.current.value
        })
        this.props.addItem(itemToAdd);
        this.closeForm();


    }
    render() {
        return (
            <div>

                <button className="open-button" onClick={this.openForm}>Add {this.props.config.itemAddName}</button>

                <div className="form-popup" id="myForm" ref={this.refForm}>
                    <form onSubmit={this.addItem} className="form-container">
                        {
                            this.props.config.inputNames.map((element,i) => {
                                return (
                                    <div>
                                        <label htmlFor={element}><b>{element}</b></label>
                                        <input ref={this.formRefs[i]} type="text" name={element} required />
                                    </div>
                                )
                            })
                        }

                        <button type="submit" className="btn">Add ...</button>
                        <button type="button" className="btn cancel" onClick={this.closeForm}>Close</button>
                    </form>
                </div>
            </div>
        )

    }


}
export default AddItem
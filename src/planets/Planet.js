import React, {Component} from 'react';
import './planets.css';
import InputPlanet from './InputPlanet';

import {BASE_URL} from '../common/constants';
import Group from '../Group';

class PlanetList extends Component{
    state = {
        planets : [],
        selectedGalaxy : this.props.galaxyId,
        selectedPlanet : 0
    }

    componentDidMount() {
        console.log("start");
        console.log(this.state.selectedGalaxy + " gekozen galaxy");
        fetch(`${BASE_URL}/planets`)
        .then(function(response) {
          return response.json();
        })
        .then(myJson => {
            this.setState({
                planets: myJson,
                selectedGalaxy : this.props.galaxyId
            });
        });
        if(this.state.planets.length !== 0){
            this.setState({
                newId: this.state.planets[this.state.planets.length-1].id + 1
            })
        }
      }

      clickOnPlanet = (planetId) => {
        fetch(`${BASE_URL}/planets/` + planetId, {
            method: 'DELETE', // or 'PUT'
            headers:{
              'Content-Type': 'application/json'
            }
          }).then(res => res.json())
          .then(response => console.log('Success:', JSON.stringify(response)))
          .catch(error => console.error('Error:', error));

          var array = [...this.state.planets];

          this.setState({
              planets : array.filter(planet => planet.id !== planetId)
          });
    }

    handleSubmit = (planet) => {
        if(planet.Name !== ""){
            // console.log(this.state.planets);
            fetch(`${BASE_URL}/planets`, {
                method: 'POST', // or 'PUT'
                body: JSON.stringify({Name: planet.Name, ImageUrl : planet.ImageUrl, GalaxyId : planet.GalaxyId}), // data can be `string` or {object}!
                headers:{
                  'Content-Type': 'application/json'
                }
              }).then(res => res.json())
              .then(response => {
                  console.log('Success:', JSON.stringify(response));
                  this.setState(prevstate =>({
                    planets: [...prevstate.planets, response]
                }));
                })
              .catch(error => console.error('Error:', error));
        }
    }

    componentWillReceiveProps(nextProp){
        this.setState({
            selectedGalaxy : nextProp.galaxyId
        });
    }

    render(){
        return (
            <div>
                <Group title="Choose your Planet">
                {
                    this.state.planets.map(planet => {
                        if (planet.GalaxyId === this.props.galaxyId) {
                            return <Planet {...planet} key={planet.id} delete={this.clickOnPlanet} />
                        } else {
                            return null
                        }
                    })
                }
                </Group>
                <InputPlanet onSubmitHandler={this.handleSubmit} availableGalaxies={this.props.availableGalaxies} newId={this.state.planets.length + 1} />
            </div>
        )
    }
}

class Planet extends Component{

    deletePlanet =  () =>{
        console.log(this.props);
        this.props.delete(this.props.id);
    }

    render(){
        return (
            <div  className="kaartje">
                <img src={this.props.ImageUrl} alt={this.props.GalaxyId} style={{margin: '20px', width:'50%'}}/>
                <div className="container">
                    <h4><b>{this.props.Name}</b></h4>
                    <p>Galaxy ID: {this.props.GalaxyId}</p>
                    <div hidden value={this.props.id}></div>
                    <button onClick={this.deletePlanet}>verwijderen</button>
                </div>
            </div>
        )
    }
}

export default PlanetList;
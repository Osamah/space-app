import React, {Component} from 'react';



class InputPlanet extends Component{

    addPlanet = () => {
        const planet = {"id" : this.props.newId , "Name" : this.newName.value, 
        "ImageUrl" : this.newUrl.value, "GalaxyId" : parseInt(this.chosenGalaxy.value, 10) }
        this.props.onSubmitHandler(planet);
        this.planet = null;
        this.newName.value = null;
        this.newUrl.value = null;
        console.log("dit is getriggered");
    }

    render(){
        return(
            <div>
                <input className="planetInput" ref = {c => this.newName = c} placeholder="aarde" required/>
                <input className="planetInput" ref = {c => this.newUrl = c} placeholder="linkadres van foto" required/>
                <select ref = {c => this.chosenGalaxy = c}>
                {
                                    this.props.availableGalaxies.map(galaxy => {
                                        return <option value={galaxy.id}>{galaxy.Name}</option>
                                    })
                }
                </select> 
                <button onClick={this.addPlanet}> Voeg planeet toe</button>
            </div>
        )
    }

}


export default InputPlanet;
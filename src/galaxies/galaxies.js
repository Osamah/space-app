import React, { Component } from 'react';
import Card from './card';
import {BASE_URL} from '../common/constants';
import AddGalaxy from './addgalaxy';
import AddItem from '../AddItem/AddItem';
import Group from '../Group';

class Galaxies extends Component{

    constructor(props) {
        super(props);

        this.state = {
            galaxies: []
        };
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.galaxies.length !== this.state.galaxies.length)
        {
            const availableGalaxies = this.state.galaxies.map(galaxy => {
                return {
                    id : galaxy.id,
                    Name : galaxy.Name
                }
            })
            this.props.setAvailableGalaxies(availableGalaxies);
        }

    }

    addToDb(newGalaxy){
        var url = BASE_URL + '/galaxies/';

        fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(newGalaxy), 
            headers:{
                'Content-Type': 'application/json'
        }
        }).then(res => res.json())
        .then(response =>
        {
            console.log('Success THIS ONE:', JSON.stringify(response));

            this.setState(prev => ({
                galaxies: [...prev.galaxies, response]
            }));
        }
        )
        .catch(error => console.error('Error:', error));
    }

    removeFromDb(galaxyId){
        var url = BASE_URL + '/galaxies/' + galaxyId;

        fetch(url, {
            method: 'DELETE', 
            body: '', 
            headers:{
                'Content-Type': 'application/json'
        }
        }).then(res => res.json())
        .then(response => {
            console.log('Success:', JSON.stringify(response));
            this.removeGalaxyFromState(galaxyId);
        })
        .catch(error => console.error('Error:', error));
    }

    addGalaxy = (newGalaxy) => {
        this.addToDb(newGalaxy);
    }

    deleteGalaxy = (galaxyId) => {
        this.removeFromDb(galaxyId);
         /* seperate method? */
    }

    removeGalaxyFromState(galaxyId){
        let currentStateGalaxies = [...this.state.galaxies];

        // delete currentStateGalaxies.id == galaxyId;

        this.setState({
            galaxies: currentStateGalaxies.filter(galaxy => galaxy.id !== galaxyId)
          })
    }

    /* Go to DB & GET galaxies */
    componentDidMount() {
        fetch(`${BASE_URL}/galaxies`)
        .then(function(response) {
          return response.json();
        })
        .then(myJson => {
            console.log(myJson);
            
          this.setState({
            galaxies: myJson,
            lastId: Array.from(myJson).length
          })
        });
      }

    render(){
      return(
            <Group title="Choose your Galaxy">
            <div className="row">
            {
                this.state.galaxies.map(galaxy =>
                    <Card id={galaxy.id} title={galaxy.Name} url={galaxy.Url} 
                    actionClickCard={this.props.actionClickCard} actionClickDelete={this.deleteGalaxy}/>
                )}
            </div>
            <AddItem config={{itemAddName:"Galaxy",inputNames:["Name","Url"]}} addItem={this.addGalaxy}></AddItem>
            </Group>
      )
    }
  }

  export default Galaxies;
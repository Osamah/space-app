import React, { Component } from 'react';
import {BASE_URL} from '../common/constants';

class AddGalaxy extends Component{

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render(){
      const {title, url} = this.props;

      return(
        <div className="add-row row">
            <div className="center">
                <label>
                    Name:
                    <input type="text" name="name" value={this.state.name} onChange={this.handleNameChange}/>
                </label>
                <label>
                    URL:
                    <input type="text" name="url" value={this.state.url} onChange={this.handleUrlChange}/>
                </label>
                <input className="btn btn-primary btn-xs" type="submit" value="Add" onClick={this.handleSubmit}></input>
            </div>
        </div>
      )
    }

    handleNameChange = (e) => {
        this.setState({name: e.target.value});
    }

    handleUrlChange = (e) =>{
        this.setState({url: e.target.value});
    }

    handleSubmit = (e) =>{
        let newGalaxy = {Name: this.state.name, Url: this.state.url};
        this.props.action(newGalaxy);
    }

    
  }

  export default AddGalaxy;
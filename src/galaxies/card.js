import React, { Component } from 'react';
import '../common/css/card.css'

class Card extends Component{

    constructor(props) {
        super(props);
        // this.addActiveClass= this.addActiveClass.bind(this);
        this.state = {
            active: false,
        };
    }

    render(){
      const {title, url, id} = this.props;

      return(
        // <div class="card" onClick={this.onClickCard} >
        //   <img class="card-img-top" src={url} alt={title} alt="Card image cap" />
        //   <div class="card-body">
        //     <h5 class="card-title">{title}</h5>
        //     <a href="#" class="btn btn-danger btn-sm" onClick={this.onClickDelete}>Delete</a>
        //   </div>
        // </div>

        <div onclick={this.toggleClass} className={'card ' + (this.state.active ? 'selected-card': null)} >
                <div class="box4 ">
                    <img src={url} alt={title} />
                    <div class="box-content">
                        <h3 class="title">{title}</h3>
                        <ul class="icon" onClick={this.onClickDelete}>
                            <li><a href="#" class="fa fa-trash white-color"></a></li>
                            <p class="white-color"><b>DELETE ?</b></p>
                        </ul>
                    </div>
                    </div>
                    <h5 class="card-title">{title}</h5>
        </div>
      )
    }

    /* Send the Id up */
    onClickCard = () =>{
      this.props.actionClickCard(this.props.id);
    }

    /* Delete a card item */
    onClickDelete = () =>{
      this.props.actionClickDelete(this.props.id);
    }

    /* Set a class to select */
    toggleClass(){
        console.log("toggleclass");
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };
  }

  export default Card;